class TesteWebjum < SitePrism::Page

    element :btn_one ,'#btn_one'
    element :btn_two ,'#btn_two'
    element :btn_four ,'#btn_link'
    element :campo_nome ,'#first_name'
    element :check_opcao_three , '#opt_three'
    element :opcao_exemplo_two , '#select_box'
    element :img_selenium , '.img-responsive-center-block'
    
    def clicar_btn_one
        btn_one.click
    end
    
    def clicar_btn_two
       btn_two.click
    end

    def clicar_btn_four
       btn_four.click
    end

    def validar_elementos_visiveis
      begin
       find(btn_one) 
       find(btn_two)   
       find(btn_four) 
      rescue => ex
        ex.message
      end
    end

    def ver_imagem_selenium
        has_xpath?("//img[contains(@src,'http://techtutorr.com/wp-content/uploads/2014/04/selenium-webdriver-online-courses-techtutorr.jpg')]")  
    end

    def preencher_nome
     campo_nome.send_keys("Ana")
    end

    def checkar_opcaoThee
       check_opcao_three.click
    end

    def selecionar_opcaoExemploTwo
       find("option[value='option_two']").click
    end

end
