Dado(/^que estou na homepage$/) do
  @home = TesteWebjum.new
  @home.visit('https://webjump-user.github.io/testqa/')
end

Dado("clicar em One") do
  @home.clicar_btn_one
end

Dado("preencher o nome") do 
  @home.preencher_nome
end

Dado("checkar OptionThree") do
  @home.checkar_opcaoThee
end

Dado("clicar em TWo") do
  @home.clicar_btn_two
end


Quando("clicar em Four") do
  @home.clicar_btn_four
end

Quando("selecionar a opção ExampleTwo") do
  @home.selecionar_opcaoExemploTwo
end

Então("devo ver a imagem do Selenium WebDriver") do
  expect(@home.ver_imagem_selenium).to eql true
end

Então("devo ve-los desaparecer") do
   @home.validar_elementos_visiveis
end
