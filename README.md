# TestQAWebJump

## Ferramentas e framework utilizados 

Capybara que é um framework  que simula um usuário no navegador junto com o selenium que comanda o driver  
Cucumber para escrita de cenários 
Site Prism para facilitar a criação de page objects
Ruby linguagem para codificação do testes 
O browser escolhido para rodar o testes foi o Chrome por ser o mais utilizado .

## Requisitos

Ruby e Bundler

## Clonando e instalando as dependências

No terminal

git clone https://AnaPaulaSS@bitbucket.org/AnaPaulaSS/testqawebjump.git

## bundle install 
vai instalar as gems que tem dentro do projeto com suas depêncdencias 

## cucumber 
Para rodar os testes no terminal 
